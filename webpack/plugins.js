const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const { SRC_DIR } = require("./paths");


console.log("html", SRC_DIR + "/index.html");

/**
 * default vue loader plugin
 */
const vueLoaderPlugin = new VueLoaderPlugin();

/**
 * Takes the template file, minifies it, and puts output in dist folder as <filename> key value
 */
const htmlWebpackPlugin = new HtmlWebpackPlugin({
    template: SRC_DIR + "/index.html",
    filename: "./index.html",
});


module.exports = {
    vueLoaderPlugin,
    htmlWebpackPlugin
};
