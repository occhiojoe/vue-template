const webpack = require('webpack');

const devtool = "#source-map";

const plugins = [
    new webpack.DefinePlugin({
        "process.env": {
            NODE_ENV: "\"production\""
        }
    }),
    new webpack.optimize.UglifyJsPlugin({
        sourceMap: true,
        compress: {
            warnings: false
        }
    }),
    new webpack.LoaderOptionsPlugin({
        minimize: true
    })
];

module.exports = {
    devtool,
    plungs
};
