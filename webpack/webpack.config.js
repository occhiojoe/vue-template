const { vueLoaderPlugin, htmlWebpackPlugin } = require("./plugins");
const { tsLoaderRule, fileLoaderRule, vueLoaderRule, txtLoaderRule, stylLoaderRule } = require("./rules");
const { SRC_DIR, DIST_DIR } = require("./paths");
const { devWebpackConfig } = require("./webpack.dev.config");
const { prodWebpackConfig } = require("./webpack.prod.config");


let config = {
    entry: SRC_DIR + "/app/index.ts", // js/ts file that all imports start from

    // where combined files are placed
    output: {
      path: DIST_DIR,
      publicPath: DIST_DIR,
      filename: "[name].bundle.js",
    },

    // how to handle files
    module: {
        rules: [
            vueLoaderRule,
            tsLoaderRule,
            // fileLoaderRule,
            // txtLoaderRule,
            stylLoaderRule,
        ],
    },

    // Plugins required to run out vue app
    // plugins: [
    //     vueLoaderPlugin,
    //     htmlWebpackPlugin,
    // ],

    // the order in which webpack looks up imports
    resolve: {
        extensions: ['.ts', '.js', '.vue', '.json'],
        alias: {
          'vue$': 'vue/dist/vue.esm.js'
        }
      },
};

// Adding config from either dev or prod files before finishing
let extraConfig = {};
if (process.env.NODE_ENV === "production") {
    extraConfig = prodWebpackConfig;
} else {
    extraConfig = devWebpackConfig;
}

config = {
    ...config,
    devtool: extraConfig.devtool,
    plugins: [
        ...config.plugins,
        extraConfig.plugins,
    ],
};

module.exports = config;
