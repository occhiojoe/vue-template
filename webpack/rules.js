const { SRC_DIR } = require("./paths");

/*
 * Loader for ts files
 */
const tsLoaderRule = {
    test: /\.tsx?$/,
    loader: "ts-loader",
    exclude: /node_modules/,
    options: {
        appendTsSuffixTo: [/\.vue$/],
    }
};

/*
 * Loader for images
 */
const fileLoaderRule = {
    test: /\.(png|jps|gif|svg)$/,
    loader: "file-loader",
    exclude: /node_modules/,
    options: {
        name: '[name].[ext]?[hash]'
    }
};

/**
 * Loader for vue files
 */
const vueLoaderRule = {
    test: /\.vue$/,
    loader: 'vue-loader',
};

/**
 * Loader for txt files, does not encode strings
 */
const txtLoaderRule = {
    test: /\.txt$/,
    loader: 'raw-loader',
    include: SRC_DIR
};

/**
 * Loader for styl files
 */
const stylLoaderRule = {
    test: /\.styl(us)?$/,
    use: ['css-loader', 'stylus-loader'],
    exclude: /node_modules/
};


module.exports = {
    tsLoaderRule,
    fileLoaderRule,
    vueLoaderRule,
    txtLoaderRule,
    stylLoaderRule,
};
