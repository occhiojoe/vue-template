const path = require('path');


/**
 * resolve source file directory, src/
 */
const SRC_DIR = path.resolve(__dirname, "./../src");

/**
 * resolve where the production build files should go, dist/
 */
const DIST_DIR = path.resolve(__dirname, "./../dist");


module.exports = {
    SRC_DIR,
    DIST_DIR
};
